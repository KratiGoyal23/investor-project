<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Fixture Match
    </h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="breadcrumb-item"><a href="#">match</a></li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="box box-default">

      <!-- /.box-header -->
      <div class="box-body wizard-content">
      <?php echo anchor(site_url('match/create'), 'Create', 'class="btn btn-primary float-start my-4"'); ?>


        <div class="col-md-4 text-center">
          <div style="margin-top: 8px" id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
          </div>
        </div>
        <div class="ibox-content">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-example">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Contest</th>
                  <th>Team First</th>
                  <th>Team Second</th>
                  <th>Match Status</th>
                  <th>Match Time</th>
                  <th>Match Type</th>
                  <th>Match Players</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach ($match_data as $match) {

                ?>
                  <tr>
                    <td width="80px"><?php echo ++$start ?></td>
                    <td width="80px"> <a href="<?php echo base_url('index.php/contest/index/' . base64_encode($match->match_id)) ?>"> <button style="border-radius: 6px;" type="button" class="btn btn-info">Contest</button> </a></td>
                    <td><?php
                        $teamone =  $this->load->Match_model->team_name($match->teamid1);

                        if (is_object($teamone) && property_exists($teamone, 'team_name') && !empty($teamone) ) {
                          echo  $teamName = $teamone->team_name;
                      } else {
                          echo 'Please Create a Team First';
                      }
                     
                        ?></td>
                    <td><?php
                        $teamtwo = $this->load->Match_model->team_name($match->teamid2);


                        if (is_object($teamtwo) && property_exists($teamtwo, 'team_name') && !empty($teamtwo  ) ) {
                          echo  $teamName = $teamtwo->team_name;
                      } else {
                        echo 'Please Create a Team Second';
                      }



                      


                        ?></td>
                    <td><?php echo $match->match_status; ?></td>
                    <td><?php echo $match->match_date_time; ?></td>
                    <td><?php echo $match->type; ?></td>

                    <td><?php echo anchor(site_url('todaymatch/players/' . base64_encode($match->match_id)), '<button style="border-radius: 6px;" type="button" class="btn btn-info"><i class="fa fa-book" aria-hidden="true"></i></button>'); ?></td>

                    <td style="text-align:center" width="200px">
                      <?php
                      echo anchor(site_url('match/read/' . $match->match_id), '<button style="border-radius: 6px;" type="button" class="btn btn-info"><i class="fa fa-book" aria-hidden="true"></i></button>');
                      echo ' ';
                      echo anchor(site_url('match/update/' . $match->match_id), '<button style="border-radius: 6px;" type="button" class="btn btn-success" ><i class="fa fa-pencil" aria-hidden="true"></i></button>');
                      echo ' ';
                      echo anchor(site_url('match/delete/' . $match->match_id), '<button style="border-radius: 6px;" type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>', 'onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                      ?>
                    </td>
                  </tr>
                <?php
                }
                ?>
              </tbody>

            </table>
          </div>
        </div>

      </div>
    </div>
</div>